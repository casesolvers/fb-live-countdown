<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<link href="https://fonts.googleapis.com/css?family=Roboto:100,100i,300,300i,400,400i,500,500i,700,700i,900,900i" rel="stylesheet">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Solovers' Cup Live Stream</title>
<style>
html, body {
	margin: 0px;
	height: 100vh;
	padding: 0px;
    color: #FFF;
	font-family: 'Roboto', sans-serif;
}
p{
	margin: 0px;
	padding: 0px;
}
.container{
	position: relative;
	height: 100%;
	background-color: transparent;
/*	background-image:url(../weblap_bg.png);
	background-size: cover;
	background-position: center;
	background-repeat: no-repeat;*/
}
.center{
	position: absolute;
	top: 40%;
	left: 50%;
	transform: translate(-50%, -50%);
	text-align: center;
	font-size: 36px;
}
.center img{
	width: 50%;
	margin-bottom: 20px;
}
.center p{
	text-shadow: -2px 1px 3px rgba(0, 0, 0, 1);
	font-weight:300;
}
#countdown{
	font-size: 48px;
	font-variant:small-caps;
	width: 500px;
}
</style>
</head>
<body>
<script>
// Set the date we're counting down to
var countDownDate = <?=strtotime('2017-09-15 14:10:00');?>;
var now = <?=time();?>;

// Update the count down every 1 second
var x = setInterval(function() {
    ++now;
    
    // Find the distance between now an the count down date
    var distance = countDownDate - now;
    
    // Time calculations for days, hours, minutes and seconds
    var days = Math.floor(distance / (60 * 60 * 24));
    var hours = Math.floor((distance % (60 * 60 * 24)) / (60 * 60));
    var minutes = Math.floor((distance % (60 * 60)) / (60));
    var seconds = Math.floor((distance % (60)));
    
    // Output the result in an element with id="demo"
    if(days == 0) {
    	days = "";
    } else {
    	days = days + "d ";
    }

    if(days == "" && hours == 0) {
    	hours = "";
    } else {
    	hours = hours + "h ";
    }

    if(hours == "" && minutes == 0) {
    	minutes = "";
    } else {
    	minutes = minutes + "m ";
    }
    document.getElementById("countdown").innerHTML = days + hours + minutes + seconds + "s ";
    
    // If the count down is over, write some text 
    if (distance < 0) {
        clearInterval(x);
        document.getElementById("countdown").innerHTML = "EXPIRED";
        window.location.href = "./redirect.php";
    }
}, 1000);
</script>
<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
<script src="js/jquery.vide.min.js"></script>
<div class="container" data-vide-bg="LS_bg/bg" >
    <div class="center">
        <img src="white_logo.png">
        <p class="title"> Opening ceremony Live Stream </p>
        <p id="countdown"></p>
    </div>
</div>
</body>
</html>
